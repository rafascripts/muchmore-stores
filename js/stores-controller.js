var app = angular.module('stores',['ngResource','angular.filter']);

app.controller('StoresCtrl', function($scope,$http) {

	$scope.search = {};
	$scope.selectedStore = null;
	$scope.categories = [];

	$http.get('https://sonaesodetapi-dev.herokuapp.com/api/v3/store?shoppingId=1').then(function(response) {
		$scope.stores = response.data;
		for(i = 0; i < $scope.stores.length; i++) {
			for(j = 0; j < $scope.stores[i].category.length; j++) {
				$scope.categories.push($scope.stores[i].category[j]);
			}
		}
		console.log($scope.stores);
	});

	$scope.toggleFavs = function() {
		if ($scope.search.anchor_store) {
			$scope.search = {};
		} else {
			$scope.search.anchor_store = true;
		}
	}

	$scope.favStore = function(store) {
		for(i = 0; i < $scope.stores.length; i++) {
			if ($scope.stores[i].id == store.id) {
				if ($scope.stores[i].anchor_store) {
					$scope.stores[i].anchor_store = false;
				} else {
					$scope.stores[i].anchor_store = true;
				}
			}
		}
	};

});